import pygame
from pygame.locals import *

from .states import MOVING, FULL

BLACK = pygame.Color([0, 0, 0])
COLOR_BG = pygame.Color([128, 128, 128])
COLOR_1 = pygame.Color([128, 255, 128])
COLOR_2 = pygame.Color([128, 128, 255])
COLOR_3 = pygame.Color([255, 128, 128])
pygame.font.init()
FONT_BIG = pygame.font.SysFont("Verdana", 60)
FONT_SMALL = pygame.font.SysFont("Verdana", 20)


class TetrisDisplay:
    # Fonts used for displaying text

    # Variables for drawing tetris glass
    glass_coords = (10, 10)
    cell_step = 40
    bg_size = 38
    block_size = 34

    # variables for drawing next tetramino box
    next_box_coords = (470, 100)
    # variables for drawing help
    next_text_coords = (470, 20)
    next_text = FONT_BIG.render("NEXT:", True, COLOR_3)
    game_over_text1_coords = (120, 400)
    game_over_text1 = FONT_BIG.render("GAME_OVER", True, COLOR_3)
    game_over_text2_coords = (180, 490)
    game_over_text2 = FONT_SMALL.render("Press space to restart", True, COLOR_3)
    help_coords = (430, 480)
    help_text = [FONT_SMALL.render("LEFT - move left", True, COLOR_3),
                 FONT_SMALL.render("RIGHT - move right", True, COLOR_3),
                 FONT_SMALL.render("UP - rotate", True, COLOR_3),
                 FONT_SMALL.render("DOWN - step down", True, COLOR_3),
                 FONT_SMALL.render("SPACE - drop", True, COLOR_3)]
    score_coords = (430, 350)

    # variables for drawing game over

    def __init__(self, display):
        """

        :param display: pygame screen
        :type display: Screen
        """
        self.display = display

    def clear(self):
        self.display.fill(BLACK)

    def draw_field(self, glass_data):
        for rownum, row in enumerate(glass_data):
            y_coord = self.glass_coords[1] + rownum * self.cell_step
            for blocknum, block in enumerate(row):
                x_coord = self.glass_coords[0] + blocknum * self.cell_step
                pygame.draw.rect(self.display, COLOR_BG,
                                 (x_coord, y_coord, self.bg_size, self.bg_size))
                if block == FULL:
                    pygame.draw.rect(self.display, COLOR_1,
                                     (x_coord + 2, y_coord + 2, self.block_size, self.block_size))
                elif block == MOVING:
                    pygame.draw.rect(self.display, COLOR_2,
                                     (x_coord + 2, y_coord + 2, self.block_size, self.block_size))
        pygame.display.update()

    def game_over(self):
        self.display.blit(self.game_over_text1, self.game_over_text1_coords)
        self.display.blit(self.game_over_text2, self.game_over_text2_coords)
        pygame.display.update()

    def draw_help(self):
        for i in range(len(self.help_text)):
            self.display.blit(self.help_text[i], (self.help_coords[0], self.help_coords[1] + i*20))
        pygame.display.update()

    def draw_score(self, score):
        """Display current score

        :param score: score (#of rows collapsed)
        :return:
        """
        pygame.draw.rect(self.display, BLACK,
                         (self.score_coords[0], self.score_coords[1], 200, 50))
        score_string = "Score: {}".format(score)
        score_text = FONT_SMALL.render(score_string, True, COLOR_3)
        self.display.blit(score_text, self.score_coords)
        pygame.display.update()

    def draw_next(self, tetramino):
        """Draw next tetramino in "next tetramino" box

        :param tetramino:
        :type tetramino: Tetramino
        :return:
        """
        self.display.blit(self.next_text, self.next_text_coords)
        pygame.draw.rect(self.display, BLACK,
                         (self.next_box_coords[0], self.next_box_coords[1], self.cell_step * 5, self.cell_step * 5))
        for rownum, row in enumerate(tetramino.figure):
            y_coord = self.next_box_coords[1] + rownum * self.cell_step
            for blocknum, block in enumerate(row):
                x_coord = self.next_box_coords[0] + blocknum * self.cell_step
                pygame.draw.rect(self.display, COLOR_BG,
                                 (x_coord, y_coord, self.bg_size, self.bg_size))
                if block == FULL:
                    pygame.draw.rect(self.display, COLOR_1,
                                     (x_coord + 2, y_coord + 2, self.block_size, self.block_size))
                elif block == MOVING:
                    pygame.draw.rect(self.display, COLOR_2,
                                     (x_coord + 2, y_coord + 2, self.block_size, self.block_size))
        pygame.display.update()
        pass
