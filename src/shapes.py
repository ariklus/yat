import numpy
from .states import EMPTY


L_SHAPE = [[2, 0, 0],
           [2, 2, 2],
           [0, 0, 0]]
L2_SHAPE = [[0, 0, 2],
            [2, 2, 2],
            [0, 0, 0]]
Z_SHAPE = [[2, 2, 0],
           [0, 2, 2],
           [0, 0, 0]]
Z2_SHAPE = [[0, 2, 2],
            [2, 2, 0],
            [0, 0, 0]]
T_SHAPE = [[0, 2, 0],
           [2, 2, 2],
           [0, 0, 0]]
I_SHAPE = [[0, 0, 0, 0],
           [2, 2, 2, 2],
           [0, 0, 0, 0],
           [0, 0, 0, 0]]
O_SHAPE = [[2, 2],
           [2, 2]]


class Tetramino:
    # How many upper rows are 0
    adjustment = 0

    def __init__(self, figure_matrix):
        """
        :param figure_matrix: 2d square matrix
        """
        for row in figure_matrix:
            if len(row) != len(figure_matrix):
                raise TypeError("Matrix representing tetris shape should be square")
        self.figure = numpy.array(figure_matrix)
        self.size = len(figure_matrix)
        # calculate adjustment needed
        nonzero = False
        for row in figure_matrix:
            for block in row:
                if block != EMPTY:
                    nonzero = True
            if nonzero:
                break
            self.adjustment -= 1

    def rotate_clockwise(self):
        self.figure = numpy.rot90(self.figure, k=-1)

    def rotate_counterclockwise(self):
        self.figure = numpy.rot90(self.figure, k=1)

    def print_figure(self):
        for row in self.figure:
            print(row)
        print('\n')
