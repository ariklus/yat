import math
import random
import pygame
from .shapes import Tetramino, T_SHAPE, I_SHAPE, O_SHAPE, Z_SHAPE, Z2_SHAPE, L_SHAPE, L2_SHAPE
from .states import EMPTY, FULL, MOVING

DEFAULT_TICK_TIME = 2000


class TetrisGlass:
    # default height and width; can be overriden
    height = None
    width = None

    active_tetramino = None
    tetra_coords = None  # Coordinates in format (row, column)
    next_tetramino = None
    score = 0
    game_over = False
    # Default shape set - currently not updatable
    available_shapes = (T_SHAPE, I_SHAPE, O_SHAPE, Z_SHAPE, Z2_SHAPE, L_SHAPE, L2_SHAPE)
    display = None
    dt = 0  # time since last tick (ms)

    def __init__(self, drawer, height=20, width=10):
        self._join_display(drawer)
        self.clock = pygame.time.Clock()
        if height:
            self.height = height
        if width:
            self.width = width
        self.glass_model = [[EMPTY for x in range(self.width)] for y in range(self.height)]
        self.tick_time = DEFAULT_TICK_TIME

    def start(self):
        self.display.clear()
        self.score = 0
        self.game_over = False
        self.glass_model = [[EMPTY for x in range(self.width)] for y in range(self.height)]
        self.tick_time = DEFAULT_TICK_TIME
        self.active_tetramino = None
        self.next_tetramino = Tetramino(random.choice(self.available_shapes))
        self.display.draw_next(self.next_tetramino)
        self.display.draw_score(self.score)
        self.display.draw_help()
        self._update()

    def print_glass(self):
        """Method used for debug"""
        for row in self.glass_model:
            print(row)
        print()

    def _check_fullness(self, starting_row=None):
        """Check if any rows are full,
        if full - remove them

        :param starting_row: Row to start checking from
        :return:
        """
        if not starting_row:
            starting_row = self.height

        for rownum in reversed(range(starting_row)):
            delrow = True
            for block in self.glass_model[rownum]:
                if block != FULL:
                    delrow = False
                    break
            if delrow:
                self._remove_row(rownum)
                self._check_fullness(starting_row=rownum+1)
        self.display.draw_score(self.score)

    def _remove_row(self, number):
        """Remove row with given id, moving all rows above 1 row down"""
        self.score += 1
        if self.tick_time > 1:
            self.tick_time = math.floor(self.tick_time * 0.99)
        for rownum in reversed(range(number+1)):
            if rownum != 0:
                self.glass_model[rownum] = self.glass_model[rownum - 1]
            else:
                self.glass_model[rownum] = [0 for x in range(self.width)]

    def _spawn_tetramino(self):
        """Attempts to spawn tetramino in the middle top of glass.
        Returns False if spawning not possible.

        :return: True if spawn is successful, False if spawn fails
        """

        self.active_tetramino = self.next_tetramino
        self.next_tetramino = Tetramino(random.choice(self.available_shapes))
        self.display.draw_next(self.next_tetramino)
        spawn_coordinates = (self.active_tetramino.adjustment, round(self.width/2 - self.active_tetramino.size/2))
        if not self._check_placement(spawn_coordinates):
            self._game_over()
            return False
        self.tetra_coords = spawn_coordinates
        self._draw_tetramino()
        self._update()
        return True

    def _check_placement(self, coordinates):
        """Check if active tetramino can fit if placed in glass in current position

        :param coordinates: Coordinates in (row, column) format
        :type coordinates: tuple
        :return:
        """
        for i in range(self.active_tetramino.size):
            for j in range(self.active_tetramino.size):
                glass_row = coordinates[0]+i
                glass_col = coordinates[1]+j
                if 0 <= glass_row < len(self.glass_model) and 0 <= glass_col < len(self.glass_model[0]):
                    if self.glass_model[glass_row][glass_col] == FULL and self.active_tetramino.figure[i][j] == MOVING:
                        return False
                else:
                    if self.active_tetramino.figure[i][j] == MOVING:
                        return False
        return True

    def _t_down(self):
        """Moves tetramino down.
        Turns tetramino into inactive block if move is not possible.
        :return True if tetramino hit the bottom; False otherwise
        :rtype: bool
        """
        new_coords = (self.tetra_coords[0]+1, self.tetra_coords[1])
        if not self._t_move(new_coords):
            self._deactivate()
            self._check_fullness()
            self._update()
            return True
        return False

    def t_left(self):
        """Moves tetramino left if possible.
        Turns tetramino into inactive block if move is not possible.
        """
        if self.active_tetramino:
            new_coords = (self.tetra_coords[0], self.tetra_coords[1]-1)
            self._t_move(new_coords)

    def t_right(self):
        """Moves tetramino left if possible.
        Turns tetramino into inactive block if move is not possible.
        """
        if self.active_tetramino:
            new_coords = (self.tetra_coords[0], self.tetra_coords[1]+1)
            self._t_move(new_coords)

    def _t_move(self, new_coords):
        """Move Tetramino to new coordinates if possible.

        :param new_coords: Coordinates tetramino should be moved to
        :return: True if move successful; False if not available
        """
        if not self._check_placement(new_coords):
            return False
        else:
            self._remove_tetramino()
            self.tetra_coords = new_coords
            self._draw_tetramino()
            self._update()
            return True

    def t_drop(self):
        """Move tetramino down till it hits the bottom"""
        if self.active_tetramino:
            hit_bottom = False
            while not hit_bottom:
                hit_bottom = self._t_down()
        self._update()

    def t_rotate(self):
        """Rotates active tetramino counterclockwise if possible"""
        if not self.active_tetramino:
            return
        self.active_tetramino.rotate_counterclockwise()
        if self._check_placement(self.tetra_coords):
            self._remove_tetramino()
            self._draw_tetramino()
            self._update()
        else:
            self.active_tetramino.rotate_clockwise()

    def _draw_tetramino(self):
        """Draws tetramino at updated coordinates (used on spawn or rotation)"""
        for i in range(self.active_tetramino.size):
            for j in range(self.active_tetramino.size):
                glass_row = self.tetra_coords[0]+i
                glass_col = self.tetra_coords[1]+j
                if 0 <= glass_row < len(self.glass_model) and 0 <= glass_col < len(self.glass_model[0]):
                    if self.glass_model[glass_row][glass_col] != 1:
                        self.glass_model[glass_row][glass_col] = self.active_tetramino.figure[i][j]

    def _remove_tetramino(self):
        """Sets tetramino blocks from MOVING to EMPTY state"""
        self._set_tetramino_state(EMPTY)

    def _deactivate(self):
        """Sets tetramino blocks from MOVING to FULL state and removes active tetramino object"""
        self._set_tetramino_state(FULL)
        self.active_tetramino = None

    def _set_tetramino_state(self, state):
        """Sets tetramino blocks from MOVING to given state

        :param state: State to set tetramino blocks to
        :return:
        """
        for i in range(self.active_tetramino.size):
            for j in range(self.active_tetramino.size):
                glass_row = self.tetra_coords[0]+i
                glass_col = self.tetra_coords[1]+j
                if 0 <= glass_row < len(self.glass_model) and 0 <= glass_col < len(self.glass_model[0]):
                    if self.glass_model[glass_row][glass_col] == MOVING:
                        self.glass_model[glass_row][glass_col] = state

    def tick(self):
        """Moves tetramino down one step if available"""
        if self.game_over:
            return
        if self.active_tetramino:
            self._t_down()
        else:
            self._spawn_tetramino()

    def check_autotick(self):
        self.dt += self.clock.tick()
        if self.dt >= self.tick_time:
            self.tick()
            self.dt = 0

    def _join_display(self, display):
        self.display = display

    def _update(self):
        if self.display:
            self.display.draw_field(self.glass_model)
        else:
            self.print_glass()

    def _game_over(self):
        self.game_over = True
        if self.display:
            self.display.clear()
            self.display.draw_field(self.glass_model)
            self.display.draw_score(self.score)
            self.display.game_over()
        else:
            print("GAME OVER")
