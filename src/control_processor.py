import sys

import pygame
from pygame.locals import *


class CommandProcessor:
    model = None

    def __init__(self, model):
        self.prev_keystate = pygame.key.get_pressed()
        self._join_model(model)

    def _join_model(self, model):
        """Attach model to send commands to

        :param model: backend model
        :type model: TetrisGlass
        :return:
        """
        self.model = model

    def get_commands(self):
        pressed_keys = pygame.key.get_pressed()
        if not self.model.game_over:
            if pressed_keys[K_LEFT] and not self.prev_keystate[K_LEFT]:
                self.model.t_left()
            if pressed_keys[K_RIGHT] and not self.prev_keystate[K_RIGHT]:
                self.model.t_right()
            if pressed_keys[K_DOWN] and not self.prev_keystate[K_DOWN]:
                self.model.tick()
            if pressed_keys[K_UP] and not self.prev_keystate[K_UP]:
                self.model.t_rotate()
            if pressed_keys[K_SPACE] and not self.prev_keystate[K_SPACE]:
                self.model.t_drop()
        else:
            if pressed_keys[K_SPACE] and not self.prev_keystate[K_SPACE]:
                self.model.start()
        self.prev_keystate = pressed_keys

    def start_game(self):
        """Run main game loop"""
        self.model.start()
        while True:
            self.get_commands()
            self.model.check_autotick()
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()


