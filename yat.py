import pygame
from src import screen_out, control_processor, tglass

pygame.init()
DISPLAYSURF = pygame.display.set_mode((700, 820))
pygame.display.set_caption("Yet Another Tetris")

# start modules: model - view - controller
drawer = screen_out.TetrisDisplay(DISPLAYSURF)

tetris = tglass.TetrisGlass(drawer)
method_list = [func for func in dir(tetris) if callable(getattr(tetris, func))]
for item in method_list:
    if "__" not in item:
        print(item)
controller = control_processor.CommandProcessor(tetris)
pressed_keys = pygame.key.get_pressed()
controller.start_game()
